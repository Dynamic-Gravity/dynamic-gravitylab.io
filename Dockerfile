FROM node:dubnium

# Create app directory
ENV APP_DIR /app/
WORKDIR ${APP_DIR}

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm ci
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

# produce build
RUN npm run build

EXPOSE 3000
CMD [ "npm", "run", "start" ]